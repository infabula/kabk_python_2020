# a global variable
vertical_line_position = 0

def setup():
    size(800, 800)
    background(255)
    frameRate(50)
    
def draw_vertical_line(start_position):
    start_x = start_position
    start_y = 0
    end_x = start_x
    end_y = height
    line(start_x, start_y, end_x, end_y)

def draw_horizontal_line(start_position):
    start_x = 0
    start_y = start_position
    end_x = width
    end_y = start_y
    line(start_x, start_y, end_x, end_y)
                     
def draw():
    global vertical_line_position
    """ Wouldn't it be nice...
    if we had a vertical blue line that starts at the left of the canvas
    and moves step by step to the right of the canvas.
    - let the line span the full heigt of the canvas
    """
    # looping in python : repetition
    # draw vertical grid lines
    background(255)
    for counter in range(0, width, 50):
        #print(counter)
        stroke(200)
        strokeWeight(1)
        draw_vertical_line(counter)
    
    # draw the horizontal grid
    for counter in range(0, height, 50):
        #print(counter)
        stroke(200)
        strokeWeight(1)
        draw_horizontal_line(counter)    
    
    # lets draw a single vertical red line
    stroke(255, 0, 0)
    strokeWeight(4)
    draw_vertical_line(vertical_line_position)
    
    vertical_line_position = vertical_line_position + 1
    print(vertical_line_position)
    
    # is the line leaving the canvas?
    if vertical_line_position > width:
        vertical_line_position = 0  # reset the position to the left side of the canvas
        
    #draw_horizontal_line(mouseY)
    
        
    
    
