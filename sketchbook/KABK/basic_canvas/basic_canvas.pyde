import random


def setup():
    # iniating stuff
    # let's define a new canvas size
    size(800, 800)
    print("Running setup")
    frameRate(1)  # call draw 1 time per second
 
def random_background():
    red = random.randint(0, 255)
    green = random.randint(0, 255)
    blue = random.randint(0, 255)
    background(blue, green, blue)
    return red, green, blue    
    
def add_image():
    img = loadImage("imageMode0.png")
    image(img, 300, 300, 100, 100)
    
          
def draw():
    r, g, b = random_background()
    fill(b, r, g)
    corner_radius = random.randint(10, 150)
    rect(200, 200, 400, 400, corner_radius)
    add_image()
    print("Draw")
